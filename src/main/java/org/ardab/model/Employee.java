package org.ardab.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import org.ardab.service.Employee.AbstractService;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;

import java.util.Objects;

@RegisterForReflection
@DynamoDbBean
public class Employee {
    private String id;
    private String name;
    private String surName;
    private Department department;
    private int employeeNumber;
    private String role;
    private String address;
    private int taxClass;
    private double grossSalaryYearly;

    public double getNetSalary() {
        return netSalary;
    }

    public void setNetSalary(double netSalary) {
        this.netSalary = netSalary;
    }

    private double netSalary;


    public Employee(){

    }

    @DynamoDbPartitionKey
    @DynamoDbAttribute(AbstractService.EMPLOYEE_ID_COL)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @DynamoDbAttribute(AbstractService.EMPLOYEE_NAME_COL)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @DynamoDbAttribute(AbstractService.EMPLOYEE_SURNAME_COL)
    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    @DynamoDbAttribute(AbstractService.EMPLOYEE_DEPARTMENT_COL)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @DynamoDbAttribute(AbstractService.EMPLOYEE_NUMBER_COL)
    public int getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    @DynamoDbAttribute(AbstractService.EMPLOYEE_ROLE_COL)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @DynamoDbAttribute(AbstractService.EMPLOYEE_ADDRESS_COL)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    @DynamoDbAttribute(AbstractService.EMPLOYEE_TAXCLASS_COL)
    public int getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(int taxClass) {
        this.taxClass = taxClass;
    }
    @DynamoDbAttribute(AbstractService.EMPLOYEE_SALARY_COL)
    public double getGrossSalaryYearly() {
        return grossSalaryYearly;
    }

    public void setGrossSalaryYearly(double grossSalaryYearly) {
        this.grossSalaryYearly = grossSalaryYearly;
    }


    public Employee(String name, String surName, Department department, int employeeNumber, String role) {
        this.name = name;
        this.surName = surName;
        this.department = department;
        this.employeeNumber = employeeNumber;
        this.role = role;
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Employee)) {
            return false;
        }

        Employee other = (Employee) obj;

        return Objects.equals(other.name, this.name);
    }
}
