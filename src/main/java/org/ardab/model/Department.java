package org.ardab.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import org.ardab.service.Department.AbstractService;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;

@RegisterForReflection
@DynamoDbBean
public class Department {
    private String id;
    private String name;
    private Department superiorDepartment;

    public Department() {

    }
    @DynamoDbAttribute(AbstractService.DEPARTMENT_NAME_COL)
    public String getName() {
        return name;
    }

    @DynamoDbPartitionKey
    @DynamoDbAttribute(AbstractService.DEPARTMENT_ID_COL)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @DynamoDbAttribute(AbstractService.DEPARTMENT_SUPERIOR_COL)
    public Department getSuperiorDepartment() {
        return superiorDepartment;
    }

    public void setSuperiorDepartment(Department superiorDepartment) {
        this.superiorDepartment = superiorDepartment;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", superiorDepartment=" + superiorDepartment +
                '}';
    }

    public Department(String name, Department superiorDepartment) {
        this.name = name;
        this.superiorDepartment = superiorDepartment;
    }
}
