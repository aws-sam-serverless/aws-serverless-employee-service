package org.ardab.exception;

import io.quarkus.resteasy.runtime.NotFoundExceptionMapper;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;

public class EmployeeNotFoundException extends NotFoundException {

    public EmployeeNotFoundException(String employeeId) {
        super("Employee doesn't exist" + employeeId);
    }


}
