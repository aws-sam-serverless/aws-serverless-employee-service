package org.ardab.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.SneakyThrows;
import org.ardab.exception.EmployeeNotFoundException;
import org.ardab.response.PayrollResponse;
import org.ardab.service.Payroll.PayrollService;

import java.util.Objects;

@Path("/payroll")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PayrollController {

    @Inject
    PayrollService payrollService;

    @SneakyThrows
    @POST
    @Path("{employeeId}")
    public PayrollResponse sendPayrollMessage(@PathParam("employeeId") String employeeId){
        return payrollService.sendPayrollMessage(employeeId);
    }
}
