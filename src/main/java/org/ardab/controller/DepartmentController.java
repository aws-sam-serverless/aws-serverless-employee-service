package org.ardab.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.ardab.model.Department;
import org.ardab.service.Department.DepartmentService;
import java.util.List;

@Path("/department")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DepartmentController {
    @Inject
    DepartmentService departmentService;

    @GET
    public List<Department> getAll() {
        return departmentService.findAll();
    }

    @GET
    @Path("{name}")
    public Department getOne(String departmentName){
        return departmentService.get(departmentName);
    }

    @POST
    public Department add(Department department){
        return departmentService.add(department);
    }
}