package org.ardab.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.ardab.exception.EmployeeNotFoundException;
import org.ardab.model.Employee;
import org.ardab.service.Employee.EmployeeService;
import software.amazon.awssdk.enhanced.dynamodb.model.GetItemEnhancedResponse;

import java.util.List;
import java.util.Objects;

@Path("/employee")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeController{
    @Inject
    EmployeeService employeeService;

    @GET
    public List<Employee> getAll() {
        return employeeService.findAll();
    }

    @GET
    @Path("{employeeId}")
    public Employee getOne(@PathParam("employeeId") String employeeId){
        return employeeService.get(employeeId);
    }

    @POST
    public Employee add(Employee employee){
        return employeeService.add(employee);
    }
}
