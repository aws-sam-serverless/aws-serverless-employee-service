package org.ardab.response;

import org.ardab.model.Employee;

public class PayrollResponse {
    private String messageId;
    private Employee employee;

    public PayrollResponse(String messageId, Employee employee) {
        this.messageId = messageId;
        this.employee = employee;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
