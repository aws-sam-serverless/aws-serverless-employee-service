package org.ardab.service.Payroll;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.ardab.model.Employee;
import org.ardab.response.PayrollResponse;
import org.ardab.service.Employee.EmployeeService;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.SendMessageResponse;

@ApplicationScoped
public class PayrollService {

    @Inject
    SqsClient sqs;

    @Inject
    EmployeeService employeeService;
    @ConfigProperty(name = "queue.url")
    String queueUrl;

    static ObjectWriter EMPLOYEE_WRITER = new ObjectMapper().writerFor(Employee.class);

    public PayrollResponse sendPayrollMessage(String employeeId) throws JsonProcessingException {
        Employee employee = employeeService.get(employeeId);
        employee.setNetSalary(calculateNetSalary(employee.getGrossSalaryYearly(), employee.getTaxClass()));
        String message = EMPLOYEE_WRITER.writeValueAsString(employee);
        SendMessageResponse response = sqs.sendMessage(m -> m.queueUrl(queueUrl).messageBody(message));
        PayrollResponse payrollResponse = new PayrollResponse(response.messageId(),employee);
        return payrollResponse;
    }

    private double calculateNetSalary(double grossSalary, int taxClass){
        double netSalary = 0;
        switch (taxClass){
            case 1:
                netSalary = grossSalary * 0.4;
            case 2:
                netSalary = grossSalary*0.5;
            case 3:
                netSalary = grossSalary*0.6;
        }
        return netSalary;
    }
}
