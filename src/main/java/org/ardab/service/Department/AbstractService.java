package org.ardab.service.Department;

import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;

public abstract class AbstractService {
    public final static String DEPARTMENT_TABLE_NAME = "department";
    public final static String DEPARTMENT_ID_COL = "departmentId";
    public final static String DEPARTMENT_NAME_COL = "departmentName";
    public final static String DEPARTMENT_SUPERIOR_COL = "departmentSuperior";


}


