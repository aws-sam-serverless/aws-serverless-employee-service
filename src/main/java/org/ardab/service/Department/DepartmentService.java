package org.ardab.service.Department;

import io.quarkus.amazon.dynamodb.enhanced.runtime.NamedDynamoDbTable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.ardab.model.Department;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;

import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class DepartmentService extends AbstractService{
    @Inject
    @NamedDynamoDbTable(DEPARTMENT_TABLE_NAME)
    DynamoDbTable<Department> departmentDynamoDbTable;

    public List<Department> findAll() {
        return departmentDynamoDbTable.scan().items().stream().collect(Collectors.toList());
    }

    public Department add(Department department){
        departmentDynamoDbTable.putItem(department);
        return department;
    }

    public Department get(String departmentName){
        Key partitionKey = Key.builder().partitionValue(departmentName).build();
        return departmentDynamoDbTable.getItem(partitionKey);
    }
}
