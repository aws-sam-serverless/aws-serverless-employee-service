package org.ardab.service.Employee;

import org.ardab.model.Employee;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractService {
    public final static String EMPLOYEE_ID_COL = "employeeId";
    public final static String EMPLOYEE_NAME_COL = "employeeName";
    public final static String EMPLOYEE_SURNAME_COL = "employeeSurname";
    public final static String EMPLOYEE_NUMBER_COL = "employeeNumber";
    public final static String EMPLOYEE_DEPARTMENT_COL = "employeeDepartment";
    public final static String EMPLOYEE_ROLE_COL = "employeeRole";
    public final static String EMPLOYEE_TABLE_NAME = "employee";

    public static final String EMPLOYEE_ADDRESS_COL = "employeeAddress";
    public static final String EMPLOYEE_TAXCLASS_COL = "employeeTaxClass";
    public static final String EMPLOYEE_SALARY_COL = "employeeSalary";
}
