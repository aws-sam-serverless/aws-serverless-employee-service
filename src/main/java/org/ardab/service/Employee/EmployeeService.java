package org.ardab.service.Employee;

import io.quarkus.amazon.dynamodb.enhanced.runtime.NamedDynamoDbTable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.NotFoundException;
import org.ardab.exception.EmployeeNotFoundException;
import org.ardab.model.Employee;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.GetItemEnhancedResponse;

import java.awt.event.FocusEvent;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class EmployeeService extends AbstractService{

    @Inject
    @NamedDynamoDbTable(EMPLOYEE_TABLE_NAME)
    DynamoDbTable<Employee> employeeDynamoDbTable;

    public List<Employee> findAll() {
        return employeeDynamoDbTable.scan().items().stream().collect(Collectors.toList());
    }

    public Employee add(Employee employee){
        employeeDynamoDbTable.putItem(employee);
        return employee;
    }

    public Employee get(String employeeId){
        Key partitionKey = Key.builder().partitionValue(employeeId).build();
        Employee employee = employeeDynamoDbTable.getItem(partitionKey);
        if(Objects.isNull(employee)){
            throw new EmployeeNotFoundException(employeeId);
        }
        else {
            return employeeDynamoDbTable.getItem(partitionKey);
        }
    }

    public boolean isExist(String employeeId){
        Key partitionKey = Key.builder().partitionValue(employeeId).build();
        return Optional.ofNullable(employeeDynamoDbTable.getItem(partitionKey)).isPresent();
    }

}
